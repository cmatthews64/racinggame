﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController : MonoBehaviour {

    public GameObject head;

    Camera mainCamera;

    void Start()
    {
        mainCamera = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {
        head.transform.position = mainCamera.transform.position;
	}
}
