﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class OpenSceneScript : MonoBehaviour {

	public void OpenScene (string sceneName) {
		SceneManager.LoadScene(sceneName);
	}
}
