﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpdateSlider : MonoBehaviour {

	public Slider slider;

	void Update() {
		float x = Input.GetAxis("Horizontal");
		slider.value += x * Time.deltaTime;
	}
}
