﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CarCamera : MonoBehaviour {

	public float speed = 1;
	public Transform driverHead;
	public Vector3 offset = new Vector3(0, 1, -5);

	void Update () {
		transform.position = Vector3.Lerp(
			transform.position,
			driverHead.position + 
			driverHead.right * offset.x + 
			driverHead.up * offset.y +
			driverHead.forward * offset.z,
			Time.deltaTime * speed);

        transform.forward = driverHead.position - transform.position;
	}
}
