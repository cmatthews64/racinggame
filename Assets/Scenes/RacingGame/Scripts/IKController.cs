﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))] 
public class IKController : MonoBehaviour {
    
    public Transform steeringWheelTarget = null;
	protected Animator animator;
 
    void Start () {
        animator = GetComponent<Animator>();
    }
    
    void OnAnimatorIK ( int layer) {
    	Transform cam = Camera.main.transform;
        animator.SetLookAtWeight(1);
        animator.SetLookAtPosition(cam.position + transform.forward);    

	    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand,1);
	    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand,1);
        animator.SetIKPosition(AvatarIKGoal.LeftHand, steeringWheelTarget.position);
        animator.SetIKRotation(AvatarIKGoal.LeftHand, steeringWheelTarget.rotation);
    }    
}

