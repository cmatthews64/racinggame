﻿using UnityEngine;
using System.Collections;

public class CrossfadeMusicOnTrigger : MonoBehaviour {

	public static AudioSource sourceA;
	public static AudioSource sourceB;

	public float fadeTime = 1;
	public AudioClip clip;
	public bool isDefaultClip = false;

	void Awake () {
		if (sourceA == null) {
			GameObject g1 = new GameObject("MusicSourceA");
			sourceA = g1.AddComponent<AudioSource>();
			sourceA.playOnAwake = false;
			GameObject g2 = new GameObject("MusicSourceB");
			sourceB = g2.AddComponent<AudioSource>();
			sourceB.playOnAwake = false;
		}

		if (isDefaultClip) {
			sourceA.clip = clip;
			sourceA.Play();
		}
	}

	void OnTriggerEnter (Collider collider) {
		StartCoroutine("FadeToClip");
	}

	IEnumerator FadeToClip () {
		sourceB.clip = clip;
		sourceB.Play();
		for (float t = 0; t < fadeTime; t += Time.deltaTime) {
			float frac = t / fadeTime;
			sourceA.volume = Mathf.Lerp(1, 0, frac);
			sourceB.volume = 1 - sourceA.volume;
			yield return new WaitForEndOfFrame();
		}
		sourceB.volume = 1;
		sourceA.Stop();
		AudioSource tmp = sourceA;
		sourceA = sourceB;
		sourceB = tmp;
	}
}
