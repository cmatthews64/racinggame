﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class SteeringWheelController : MonoBehaviour {

	public CarController car;

	void Update () {
		Vector3 rot = transform.localEulerAngles;
		rot.z = -2 * car.CurrentSteerAngle;
		transform.localEulerAngles = rot;
	}
}
