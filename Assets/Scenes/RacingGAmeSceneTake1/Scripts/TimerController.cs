using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour {
	
	public Text timerText;
	public float timeRemaining;
	private bool startTimer;

	void Start() {
		//timeRemaining = 10;
	}
	
	void Update() {
		if (timeRemaining <= 0 && startTimer) {
			SendMessage("TimeHasElapsed", SendMessageOptions.DontRequireReceiver);
		}
		else if (startTimer) {
			timeRemaining -= Time.deltaTime;
			SetText(timeRemaining);
		}	
	}

	void StartTimer() {
		SetText(timeRemaining);
		startTimer = true;
	}

	void Win(){
		startTimer = false;
		timerText.text = "You Won!";
        Debug.Log("You Win!");
		BroadcastMessage("StartRestartCountDown");
	}

	void Lose() {
		startTimer = false;
		timerText.text = "Time's up!\nYou Lose";
		BroadcastMessage("StartRestartCountDown");
	}

	void TimeIncrease(float amount) {
		timeRemaining += amount;
        timerText.text = "You Win!";
	}

	void SetText(float amount) {
		timerText.text = "Time: " + amount.ToString("F2");
	}

}
