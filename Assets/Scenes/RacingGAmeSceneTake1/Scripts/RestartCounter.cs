﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartCounter : MonoBehaviour {
	 int time,a;
	 float x;
	 public bool count;
	 public string timeDisp;

	 void Start () {
	     time = 3;
	     count = false;
	 }
	 
	 // Update is called once per frame
	 void FixedUpdate (){
	     if (count){
	         timeDisp = time.ToString ();
	         timeDisp = GameObject.Find ("RestartCounter").GetComponent<Text>().text;
	         x += Time.deltaTime;
	         a = (int)x;
	         switch(a){
	             case 0: GameObject.Find ("RestartCounter").GetComponent<Text> ().text = "3"; break;
	             case 1: GameObject.Find ("RestartCounter").GetComponent<Text> ().text = "2"; break;
	             case 2: GameObject.Find ("RestartCounter").GetComponent<Text> ().text = "1"; break;
	             case 3: RestartGame();
                 count = false; break;
	         }
	     }
	 }

	 void StartRestartCountDown() {
	 	count = true;
	 }

	void RestartGame() {
		GameObject.Find ("RestartCounter").SetActive(false);
		Application.LoadLevel("NewRacingTrack");
	}
}
