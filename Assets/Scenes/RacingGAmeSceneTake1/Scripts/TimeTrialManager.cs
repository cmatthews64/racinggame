﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeTrialManager : MonoBehaviour {

	public float timeIncrease = 5;
	private bool timeElapsed = false;
	private int ringsTotal;
	private int ringsCollected;

	void Awake() {
		 //Gather how many rings are remaining
		 RingController[] rings = GameObject.FindObjectsOfType<RingController>() as RingController[];
		 ringsTotal = rings.Length;
		 //Debug.Log("rings: " + ringsTotal);
	}

	void Update() {	 
		 if (ringsCollected == ringsTotal * 5) {
		 	SendMessage("Win", SendMessageOptions.DontRequireReceiver);
		 }

		 if (timeElapsed) {
		 	SendMessage("Lose", SendMessageOptions.DontRequireReceiver);
		 }
	}

	//If the game controller receives this signal from the timer, it will end the game
	void TimeHasElapsed() {
	 timeElapsed = true;
	}

	//If the Game Controller receives this signal from a destroyed item,
	//  it sends a message to the time object to increase the time left and increment the count.
	void RingCollected() {
	 IncreaseTime();
	 ringsCollected += 1;
        Debug.Log(ringsCollected);
	 //Debug.Log("rings: " + ringsCollected);
	}

	void IncreaseTime() {
	 SendMessage("TimeIncrease", timeIncrease, SendMessageOptions.DontRequireReceiver);
	}


}
	//I've included this dead function because I can't test the code myself right now and I don't want to leave
	// you with errors. IT may or may not be needed, though.
