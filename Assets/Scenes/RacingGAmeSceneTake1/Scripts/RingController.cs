﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour {
	public bool isFirstRing;

	private Animator animator;
 
    void Start () {
        animator = GetComponent<Animator>();
    }

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			if (isFirstRing) {
				SendMessageUpwards("StartTimer", SendMessageOptions.DontRequireReceiver);
			}
			SendMessageUpwards("RingCollected", SendMessageOptions.DontRequireReceiver);
			animator.SetTrigger("ringCollected");
		}
	}

}
