﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Checkpoints
{
    
    public GameObject CheckpointObject;
    public Checkpoints prevCheckpoint;
    public Checkpoints nextCheckpoint;
    public Checkpoints postCheckpoint;
    public Checkpoints penCheckpoint;
    public bool hasCollected = false;
    public bool firstCheckpoint =  false;
    public bool lastCheckpoint =  false;
    
}

public class CheckpointTrailManager : MonoBehaviour {

    public Checkpoints[] checkpointArray;
    private void Start()
    {
        for (int i = 0; i < checkpointArray.Length; i++)
        {
            checkpointArray[i].CheckpointObject.SetActive(false);
            checkpointArray[i].hasCollected = checkpointArray[i].CheckpointObject.GetComponent<CheckpointController>().hasCollected;
        }

        for (int i = 0; i < checkpointArray.Length; i++)
        {
            
            if (i == 0)
            {
                checkpointArray[i].firstCheckpoint = true;
                checkpointArray[i].nextCheckpoint = checkpointArray[i + 1];
                checkpointArray[i].postCheckpoint = checkpointArray[i + 2];
                checkpointArray[i].CheckpointObject.SetActive(true);
                checkpointArray[i].nextCheckpoint.CheckpointObject.SetActive(true);
            }
            else if (i == 1)
            {
                checkpointArray[i].prevCheckpoint = checkpointArray[i - 1];
                
                checkpointArray[i].nextCheckpoint = checkpointArray[i + 1];
                checkpointArray[i].postCheckpoint = checkpointArray[i + 2];
            }
            else if (i == checkpointArray.Length - 1)
            {
                checkpointArray[i].lastCheckpoint = true;
                checkpointArray[i].prevCheckpoint = checkpointArray[i - 1];
                checkpointArray[i].penCheckpoint = checkpointArray[i - 2];
                checkpointArray[i].CheckpointObject.SetActive(false);
            }
            else if (i == checkpointArray.Length - 2)
            {
                checkpointArray[i].prevCheckpoint = checkpointArray[i - 1];
                checkpointArray[i].penCheckpoint = checkpointArray[i - 2];
                checkpointArray[i].nextCheckpoint = checkpointArray[i + 1];
                
            }
            else
            {
                checkpointArray[i].prevCheckpoint = checkpointArray[i - 1];
                checkpointArray[i].penCheckpoint = checkpointArray[i - 2];
                checkpointArray[i].nextCheckpoint = checkpointArray[i + 1];
                checkpointArray[i].postCheckpoint = checkpointArray[i + 2];
                

            }
            Debug.Log("Checkpoint " + (i + 1) + " active = " + checkpointArray[i].CheckpointObject.active);
            
        }
    }
    // Update is called once per frame
    void Update () {
        for (int i = 0; i < checkpointArray.Length; i++)
        {
            Checkpoints prevCheckpoint = checkpointArray[i].prevCheckpoint;
            Checkpoints penCheckpoint = checkpointArray[i].penCheckpoint;
            
            Checkpoints currentCheckpoint = checkpointArray[i];

            Checkpoints nextCheckpoint = checkpointArray[i].nextCheckpoint;
            Checkpoints postCheckpoint = checkpointArray[i].postCheckpoint;

            checkpointArray[i].hasCollected = checkpointArray[i].CheckpointObject.GetComponent<CheckpointController>().hasCollected;

            if (checkpointArray[i].hasCollected)
            {
                if (checkpointArray[i].lastCheckpoint)
                {
                    //you've finished
                    Debug.Log("You Win");
                    currentCheckpoint.CheckpointObject.SetActive(false);
                }
                else if(checkpointArray[i] == checkpointArray[checkpointArray.Length - 2])
                {
                    currentCheckpoint.CheckpointObject.SetActive(false);
                }
                else
                {
                    nextCheckpoint.CheckpointObject.SetActive(true);
                    postCheckpoint.CheckpointObject.SetActive(true);
                    currentCheckpoint.CheckpointObject.SetActive(false);
                }
            }
        }
	}
}
