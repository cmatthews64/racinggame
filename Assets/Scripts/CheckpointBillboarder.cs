﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointBillboarder : MonoBehaviour {

    public Transform cameraSubject;
	
	// Update is called once per frame
	void Update () {

        Vector3 v = Camera.main.transform.position - transform.position;
        v.x = v.z = 0.0f;
        

        transform.LookAt(cameraSubject.position - v);
	}
}
