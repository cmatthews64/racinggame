﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class GaugesController : MonoBehaviour {

	public CarController car;

	public Transform rpmNeedle;
	public Transform mphNeedle;

	void Update () {
		mphNeedle.transform.localEulerAngles = Vector3.up * Mathf.Lerp(0, 315, car.CurrentSpeed / car.MaxSpeed);
		rpmNeedle.transform.localEulerAngles = Vector3.up * Mathf.Lerp(0, 315, car.Revs);
	}
}
