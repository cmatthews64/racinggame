﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour {
    
    public bool hasCollected = false;

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            hasCollected = true;
            Debug.Log("Collected Checkpoint");
        }
    }

}
